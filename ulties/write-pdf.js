const fs = require("fs");
const path = require("path");
const _ = require("lodash")
const XLSX = require("xlsx-style");
var wkhtmltopdf = require('wkhtmltopdf');
var ncp = require('ncp').ncp;
const JSZip     = require("jszip");
const Mustache  = require("mustache");
ncp.limit = 16;

function writeToPDF(template, outputFileName, replace = {}, CompleteOne = false) {
    var html = fs.readFileSync(template, 'utf8');
    _.forOwn(replace, function (value, key) {
        html = html.replace(new RegExp(`{{${key}}}`,"g"), value);
    });
    let tmpFileName = path.join(global.config.tmpPath,(new Date()).getTime()+".html");
    fs.writeFileSync(tmpFileName, html);
    wkhtmltopdf("file:///" + tmpFileName, { 
        output: outputFileName,
        printMediaType: true,
        marginLeft: '15mm',
        marginRight : '15mm',
        marginTop : '15mm',
        marginBottom : '15mm',
        pageOffset: 0,
        footerCenter: 'page [page]/[toPage]',
        orientation: "Landscape",

    }, function() {
        console.log(path.basename(outputFileName, '.pdf'));
        fs.unlinkSync(tmpFileName);
        global.processLogger.emit('completeOne', CompleteOne);
    });
}
async function writeXLSX(receipt) {
    source = global.config.xlsxTemplate;
    destination = path.join(global.config.xlsxPath, receipt.number);
    let template = fs.readFileSync(path.join(global.config.templatePath, 'row.xml'), 'utf-8');
    let formatedDate = `${_.padStart(receipt.date.getDate(),2,"0")}-${_.padStart(receipt.date.getMonth()+1, 2, "0")}-${receipt.date.getFullYear()}`;
    ncp(source, destination, function (err) {
        let zip = new JSZip();
        let rowIndex = 12;
        let rowData = "";
        receipt.IN.item.forEach(item => {
            rowData += Mustache.render(template, {
                row: rowIndex,
                date: formatedDate,
                userName: item.name,
                address: item.address,
                productName: item.productName,
                qty: item.productQty,
                priceBeforTax: item.totalPrice,
                tax: item.tax,
                priceAfterTax: item.totalAfterTax,
                note: ""
            })
            rowIndex ++
        });
        receipt.GIFT.item.forEach(item => {
            rowData += Mustache.render(template, {
                row: rowIndex,
                date: formatedDate,
                userName: item.name,
                address: item.address,
                productName: item.productName,
                qty: item.productQty,
                priceBeforTax: item.totalPrice,
                tax: item.tax,
                priceAfterTax: item.totalAfterTax,
                note: item.note ? item.note : ""
            })
            rowIndex ++
        });
        let fileTemplate = fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/sheet1.xml"), 'utf-8');
        fileTemplate = Mustache.render(fileTemplate, {
            rowData,
            receiptNumber: receipt.number,
            rowAfterData1: rowIndex,
            rowAfterData2: rowIndex+1,
            rowAfterData3: rowIndex+2,
            rowAfterData4: rowIndex+3,
            rowAfterData5: rowIndex+4,
        })
        fs.writeFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/sheet1.xml"), fileTemplate)
        zip
            .file("[Content_Types].xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "[Content_Types].xml")))
            .file("_rels/.rels", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "_rels/.rels")))
            .file("docProps/app.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "docProps/app.xml")))
            .file("docProps/core.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "docProps/core.xml")))
            .file("docProps/custom.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "docProps/custom.xml")))
            .file("xl/workbook.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/workbook.xml")))
            .file("xl/styles.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/styles.xml")))
            .file("xl/sharedStrings.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/sharedStrings.xml")))
            .file("xl/_rels/workbook.xml.rels", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/_rels/workbook.xml.rels")))
            .file("xl/drawings/drawing1.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/drawings/drawing1.xml")))
            .file("xl/drawings/drawing2.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/drawings/drawing2.xml")))
            .file("xl/worksheets/sheet1.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/sheet1.xml")))
            .file("xl/worksheets/sheet2.xml", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/sheet2.xml")))
            .file("xl/worksheets/_rels/sheet1.xml.rels", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/_rels/sheet1.xml.rels")))
            .file("xl/worksheets/_rels/sheet2.xml.rels", fs.readFileSync(path.join(global.config.xlsxPath, receipt.number, "xl/worksheets/_rels/sheet2.xml.rels")))
        zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
        .pipe(fs.createWriteStream(path.join(global.config.xlsxPath, receipt.number+'.xlsx')))
        .on('finish', function () {
            console.log(receipt.number+'.xlsx');
            global.processLogger.emit('completeOne', "completeXLSX");
        });
    });
}
module.exports = {
    write: writeToPDF,
    writeXLSX
}