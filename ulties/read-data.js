const XLSX = require("xlsx");
const { getJsDateFromExcel } = require('excel-date-to-js');
const convertVietnamese = require('../convert-vietnamese');
const _ = require("lodash");

function readData (filePath) {
    console.log("Reading file ...");
    let workBook = XLSX.readFile(filePath);
    console.log("Complete read file");
    return workBook;
}

function getReceiptList(workBook) {
    let tmpList = XLSX.utils.sheet_to_json(
        workBook.Sheets['bravo60'], 
        {
            range: workBook.Sheets['bravo60']['!ref'].replace("A1", "A9"),
            header: [
                "stt",
                "skipCode",
                "date",
                "receipt_number",
                "desc",
                "productCode",
                "warehouse",
                "qty",
                "price",
                "totalPrice",
                "tkDebt",
                "tkHave",
                "tax"
            ]
        });
    let returnList = {};
    tmpList.forEach(item => {
        if (!item.receipt_number) {
            return
        }
        if (returnList[item.receipt_number] === undefined) {
            returnList[item.receipt_number] = {}
        }
        if (item.stt != 0) {
            returnList[item.receipt_number]['total'] = item.totalPrice + item.tax
            returnList[item.receipt_number]['date'] = getJsDateFromExcel(item.date)
        } else {
            if (returnList[item.receipt_number]['item'] === undefined) {
                returnList[item.receipt_number]['item'] = {}
            }
            if (typeof returnList[item.receipt_number]['item'][item.productCode] === "undefined") {
                returnList[item.receipt_number]['item'][item.productCode] = {
                    qty: item.qty,
                    price: item.totalPrice/item.qty,
                    // price: item.price,
                    tax: item.tax/item.qty,
                    name: convertVietnamese.toUnicode(item.desc).trim()
                }
            } else {
                returnList[item.receipt_number]['item'][item.productCode].qty += item.qty;
            }
        }
    })
    return returnList
}

function getProductList(workBook) {
    tmpList = XLSX.utils.sheet_to_json(workBook.Sheets['DM']);
    returnList = {};
    tmpList.forEach(item => {
        returnList[item['M· vËt t­']] = convertVietnamese.toUnicode(item['Tªn vËt t­'])
    })
    return returnList;
}

function getUserList(workBook) {
    tmpList = XLSX.utils.sheet_to_json(workBook.Sheets['Sheet']);
    returnList = [];
    tmpList.forEach(item => {
        if ("kg nhap mã này" == item["Tên KH"] || item["Tên KH"] == false) {
            return
        }
        if (!item["Tên KH"] || !item["Địa chỉ"]) {
            return
        }
        returnList.push({
            name: item["Tên KH"],
            address: item["Địa chỉ"],
        })
    })
    return returnList;
}

module.exports = {
    readData,
    getReceiptList,
    getProductList,
    getUserList,
    processData: function(filePath) {
        let workBook = this.readData(filePath);
        console.log("analysys data .....");
        let receiptList = this.getReceiptList(workBook);
        let productList = this.getProductList(workBook);
        let userListt = this.getUserList(workBook);
        console.log("Complete analysys");
        return {
            receiptList,
            productList,
            userListt
        }
    }
}
