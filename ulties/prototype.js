const _ = require("lodash");
Number.prototype.currency = function(delimiter = ",") {
    let number = this;
    number = number.toString().split(".");
    let beforeDot = number[0].split("").reverse();
    

    let returnNumer = [];
    beforeDot.forEach((element, index) => {
        if (index > 0 && index%3 === 0) {
            returnNumer.push(delimiter)
        }
        returnNumer.push(element)
    });

    let returnAfterDot = [];
    if (number[1] !== undefined) {
        let afterDot = number[1].split("");
        afterDot.forEach((element, index) => {
            if (index > 0 && index%3 === 0) {
                returnAfterDot.push(delimiter)
            }
            returnAfterDot.push(element)
        });
    }
    return returnNumer.reverse().join("") +  (returnAfterDot.join("") ? ("." + returnAfterDot.join("")) : "");
};

Number.prototype.devideToInt = function (numberToDevide) {
    let returnArray = [];
    for (let i = 1; i <= numberToDevide; i++) {
        returnArray.push(0);
    }
    let number = this;
    let average = number/numberToDevide;
    let max = _.ceil(average);
    let min = _.floor(average);
    if (max === 0) {
        return returnArray;
    } else if (max === 1) {
        for (let i = 0; i < number; i++) {
            returnArray[i] = max;
        }
        return returnArray;
    } else {
        let i = 0;
        let tmpNumberToDevide = numberToDevide;
        while (number/min !== tmpNumberToDevide) {
            returnArray[i] = max;
            number -= max;
            i++;
            tmpNumberToDevide--;
        }
        while(i < numberToDevide) {
            returnArray[i] = min;
            i++;
        }
        return returnArray;
    }
};

Number.prototype.differenceWith = function (number) {
    return Math.abs(this - number);
}