const path = require("path");
module.exports = {
    templatePDF: path.join(__dirname, "template", "template.html"),
    tmpPath: path.join(__dirname, "tmp"),
    pdfPath: path.join(__dirname, "pdf"),
    xlsxTemplate: path.join(__dirname, "template", 'xlsx'),
    templatePath: path.join(__dirname, "template"),
    xlsxPath: path.join(__dirname, "tmp", 'xlsx'),
    completeIN: 0,
    completeKM: 0,
    completeXLSX: 0,
    maxItemPerUser: 1,
    style: {
        borderAll: {
            top: {
                style: "thin",
                color: "000000",
            },
            bottom: {
                style: "thin",
                color: "000000",
            },
            left: {
                style: "thin",
                color: "000000",
            },
            right: {
                style: "thin",
                color: "000000",
            },
        }
    },
    rowTemplate: `<tr>
                    <td>{{date}}</td>
                    <td>{{name}}</td>
                    <td>{{address}}</td>
                    <td style="white-space: nowrap">{{productName}}</td>
                    <td style="white-space: nowrap; text-align: center">{{productQty}}</td>
                    <td style="white-space: nowrap; text-align: right">{{totalPrice}}</td>
                    <td style="white-space: nowrap; text-align: right">{{tax}}</td>
                    <td style="white-space: nowrap; text-align: right">{{totalAfterTax}}</td>
                    <td>{{note}}</td>
                </tr>`
};