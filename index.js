#!/usr/bin/env node
let Config = require("./config");
Object.defineProperty(global, 'config', {
    get: function(){
        return Config;
    }
});
const eventEmitter = require("events");
global.processLogger = new eventEmitter();
const argv      = require("./agrv");
const readData  = require("./ulties/read-data");
const writePDF  = require("./ulties/write-pdf");
const _         = require("lodash");
const log       = require("./log/log");
const path      = require("path");
const fs        = require("fs");
const readline  = require('readline');
const JSZip     = require("jszip");
const Mustache  = require("mustache");
var rimraf      = require("rimraf");
require("./ulties/prototype");
let CountReceipt = 0;

// for screen not close after complete
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.question('', () => {rl.close()});

if(!fs.existsSync(global.config.pdfPath)){
    fs.mkdirSync(global.config.pdfPath)
}
if(!fs.existsSync(global.config.tmpPath)){
    fs.mkdirSync(global.config.tmpPath)
}
if(!fs.existsSync(path.join(__dirname, "tmp", "log"))){
    fs.mkdirSync(path.join(__dirname, "tmp", "log"));
}

fs.existsSync(path.join(__dirname, "tmp", "log", "error.txt")) && fs.unlinkSync(path.join(__dirname, "tmp", "log", "error.txt"));
fs.existsSync(path.join(__dirname, "tmp", "log", "log.txt")) && fs.unlinkSync(path.join(__dirname, "tmp", "log", "log.txt"));
fs.existsSync(path.join(__dirname, "tmp", "log", "warn.txt")) && fs.unlinkSync(path.join(__dirname, "tmp", "log", "warn.txt"));

// Clear tmp file before start
let PDFdirObject = fs.readdirSync(global.config.pdfPath);
let XLSXdirObject = fs.readdirSync(global.config.xlsxPath);
XLSXdirObject.forEach(function (filename, index) {
    if (!filename.match(/^.*\.xlsx/i)) {
        return
    }
    fs.unlinkSync(path.join(global.config.xlsxPath, filename))
});
PDFdirObject.forEach(function (filename, index) {
    if (!filename.match(/^.*\.pdf/i)) {
        return
    }
    fs.unlinkSync(path.join(global.config.pdfPath, filename))
});

global.processLogger.on("completeOne", function (data) {
    global.config[data]++;
    if (global.config.completeIN == CountReceipt && global.config.completeKM == CountReceipt && global.config.completeXLSX == CountReceipt) {
        console.log("Complete write file PDF and XLSX");
        console.log("Generating Zip file");

        let zip = new JSZip();

        let PDFdirObject = fs.readdirSync(global.config.pdfPath);
        PDFdirObject.forEach(function (filename, index) {
            if (!filename.match(/^.*\.pdf/i)) {
                return
            }
            zip.folder("PDF").file(filename, fs.readFileSync(path.join(global.config.pdfPath, filename)));
        });
        
        let XLSXdirObject = fs.readdirSync(global.config.xlsxPath);
        XLSXdirObject.forEach(function (filename, index) {
            if (!filename.match(/^.*\.xlsx/i)) {
                return
            }
            zip.folder("EXCEL").file(filename, fs.readFileSync(path.join(global.config.xlsxPath, filename)));
        });

        zip.file("data.json", JSON.stringify(jsonOut, null, 2));
        if (fs.existsSync(path.join(__dirname, "tmp", "log", "log.txt"))) {
            zip.file("log.txt", fs.readFileSync(path.join(__dirname, "tmp", "log", "log.txt")));
            fs.unlinkSync(path.join(__dirname, "tmp", "log", "log.txt"));
        }
        if (fs.existsSync(path.join(__dirname, "tmp", "log", "error.txt"))) {
            zip.file("error.txt", fs.readFileSync(path.join(__dirname, "tmp", "log", "error.txt")));
            fs.unlinkSync(path.join(__dirname, "tmp", "log", "error.txt"));
        }
        if (fs.existsSync(path.join(__dirname, "tmp", "log", "warn.txt"))) {
            zip.file("warn.txt", fs.readFileSync(path.join(__dirname, "tmp", "log", "warn.txt")));
            fs.unlinkSync(path.join(__dirname, "tmp", "log", "warn.txt"));
        }

        zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
        .pipe(fs.createWriteStream(argv.f+".zip"))
        .on('finish', function () {
            console.log("Clearing tmp file")
            XLSXdirObject.forEach(function (filename, index) {
                rimraf.sync(path.join(global.config.xlsxPath, filename));
            });
            PDFdirObject.forEach(function (filename, index) {
                if (!filename.match(/^.*\.pdf/i)) {
                    return
                }
                fs.unlinkSync(path.join(global.config.pdfPath, filename))
            });
            console.log("generated xlsx Zip file.", "completed Press Enter to exit");
        });
    }
});

let processdata = readData.processData(argv.f);
let tmpUserList = _.clone(processdata.userListt);
CountReceipt = Object.keys(processdata.receiptList).length
let jsonOut = {};
let previousReceiptIndex = 0;
let currentReceiptIndex = 0;
_.forOwn(processdata.receiptList, function(receipt, receipt_number) {
    currentReceiptIndex++;

    // prepare variable
    let listUserIN = [];
    let listUserKM = [];
    let INitem = {};
    let KMitem = {};
    let GIFTitem = {};
    let PDitem = {};
    let listItemKmPerGas = [];
    let totalGasQty = 0;
    let totalKMQty = 0;
    let totalGIFTQty = 0;
    let totalPDQty = 0;
    let formatedDate = `${_.padStart(receipt.date.getDate(),2,"0")}-${_.padStart(receipt.date.getMonth()+1, 2, "0")}-${receipt.date.getFullYear()}`;
    let fileNameIN = `${receipt_number}_${formatedDate}_IN.pdf`;
    let fileNameKM = `${receipt_number}_${formatedDate}_KM.pdf`;
    
    // prepare json object to write data
    jsonOut[receipt_number] = {
        date: receipt.date,
        total: receipt.total,
        number: receipt_number,
        IN: {
            total: 0,
            item: []
        },
        KM: {
            total: 0,
            item: []
        },
        GIFT: {
            total: 0,
            item: []
        },
    };

    //Check empty string
    if (!receipt_number) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Số hóa đơn bị rỗng, sau hóa đơn: ${previousReceiptIndex}\r\n\r\n`);
    }
    if (isNaN(receipt.date.getTime())) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Ngày xuất hóa đơn không hợp lệ\r\n\r\n`);
    }

    // devide item to KM, IN, GIFT
    _.forOwn(receipt.item, function(productDetail, productCode) {
        if (!Number.isInteger(productDetail.qty)) {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Số lượng sản phẩm không phải là số nguyên, code: ${productCode}, số lượng: ${productDetail.qty} \r\n\r\n`);
        }
        if (productCode.match(/^PD.*/i)) {
            PDitem[productCode] = productDetail;
            totalPDQty += productDetail.qty;
        } else if (productCode.match(/^[GBP].*/i)) {
            INitem[productCode] = productDetail;
            totalGasQty += productDetail.qty;
        } else if (productDetail.price > 0) {
            KMitem[productCode] = productDetail;
            totalKMQty += productDetail.qty;
        } else {
            GIFTitem[productCode] = productDetail;
            totalGIFTQty += productDetail.qty;
        }
    });
    
    // check user list is out of user
    if (tmpUserList.length < totalGasQty) {
        tmpUserList = tmpUserList.concat(processdata.userListt);
    }

    // devide KM item per person
    _.forOwn(KMitem, function (productDetail, productCode) {
        let tmpList = productDetail.qty.devideToInt(totalGasQty);
        tmpList.forEach(function(qty, index) {
            if (listItemKmPerGas[index] === undefined) {
                listItemKmPerGas[index] = {}
            }
            listItemKmPerGas[index][productCode] = {qty, price: productDetail.price, name: productDetail.name}
        })
    });

    // devide IM item to user, change in random to change max item per user
    _.forOwn(INitem, function (productDetail, productCode) {
        productQty = productDetail.qty
        while (productQty > 0) {
            let user = _.pullAt(tmpUserList, [1])[0];
            if (productQty < (global.config.maxItemPerUser+1)) {
                listUserIN.push({
                    username: user.name,
                    address: user.address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: productQty,
                    price: productDetail.price,
                    tax: productDetail.tax,
                });
                productQty = 0;
            } else {
                getProductQty = _.random(1, global.config.maxItemPerUser);
                listUserIN.push({
                    username: user.name,
                    address: user.address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: getProductQty,
                    price: productDetail.price,
                    tax: productDetail.tax,
                });
                productQty = productQty - getProductQty;
            }
            if (!user.name || !user.address) {
                fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Thông tin người dùng bị rỗng\r\n\r\n`);
            }
        }
    });

    //prepare string to write to sheet IN
    let tableBody = "";
    let totalIN = 0;
    let testTotalIN = 0;
    let totalQty = 0;
    listUserIN.forEach(function(rowData, index) {
        let tpmKMList = {}
        for (let i = rowData.productQty; i > 0; i--) {
            let valuePop = listItemKmPerGas.pop();
            _.forOwn(valuePop, function (product, productCode) {
                if (tpmKMList[productCode] == undefined) {
                    tpmKMList[productCode] = product;
                } else {
                    tpmKMList[productCode].qty += product.qty
                }
            })
        }
        _.forOwn(tpmKMList, function (product, productCodeKM) {
            listUserKM.push({
                username: rowData.username,
                address: rowData.address,
                productCode: productCodeKM,
                productName: product.name,
                productQty: product.qty,
                price: product.price,
            })
        });

        // create row table IN.
        let totalPrice = rowData.price*rowData.productQty
        let tax = rowData.tax*rowData.productQty
        let totalAfterTax = 
            (listUserKM.length > 0) ? 
            _.round(totalPrice + tax) : 
            (totalPrice + tax)
        let roundedPriceAftertax = _.round(totalPrice + tax,-1);
        if (Math.abs(totalAfterTax-roundedPriceAftertax) <= 1 && !rowData.productCode.match(/^[B].*/i)) {
            totalAfterTax = roundedPriceAftertax;
        }
        totalQty += rowData.productQty;
        testTotalIN += _.round(totalAfterTax);
        totalIN += totalAfterTax;
        tableBody += Mustache.render(global.config.rowTemplate, {
            date: `${receipt.date.getDate()}/${receipt.date.getMonth()+1}/${receipt.date.getFullYear()}`,
            name: rowData.username,
            address: rowData.address, 
            productCode: rowData.productCode,
            productName: rowData.productName,
            productQty: rowData.productQty,
            productPrice: _.round(rowData.price).currency(),
            totalPrice: _.round(totalPrice).currency(),
            tax: _.round(tax).currency(),
            totalAfterTax: _.round(totalAfterTax).currency()
        });
        jsonOut[receipt_number]['IN']['item'].push({
            name: rowData.username,
            address: rowData.address,
            productCode: rowData.productCode,
            productName: rowData.productName,
            productQty: rowData.productQty,
            productPrice: rowData.price,
            totalPrice: totalPrice,
            tax: tax,
            totalAfterTax: totalAfterTax
        });

        //Check empty string
        if (!rowData.username || !rowData.address || !rowData.productCode || !rowData.productName || !rowData.price || !totalPrice || !tax || !totalAfterTax) {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Dữ liệu bị rỗng ở Sheet IN\r\n\r\n`);
        }
        if (_.round(totalAfterTax)%100 != 0) {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "warn.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tiền sản phẩm bị lẻ: code: ${rowData.productCode}, ${rowData.productName}, tổng tiền sau thuế: ${_.round(totalAfterTax).currency()} \r\n\r\n`);
        }
    });

    let totalGasPipeQty = totalPDQty;
    let totalUserIN = listUserIN.length;
    let userIndex = 0;
    _.forOwn(PDitem, function (productDetail, productCode) {
        if (totalUserIN > 0) {
            let maxGasPipeQtyPerUser = Math.ceil(totalGasPipeQty/totalUserIN);
            let UserQtyUseThisProduct = Math.ceil(productDetail.qty/maxGasPipeQtyPerUser);
            while (UserQtyUseThisProduct > 0) {
                if (productDetail.qty === 0) {
                    break;
                }
                let PipeQty = maxGasPipeQtyPerUser;
                if (PipeQty > productDetail.qty) {
                    PipeQty = productDetail.qty;
                }
                let totalPrice = productDetail.price*PipeQty;
                let tax = productDetail.tax*PipeQty;
                let totalAfterTax = _.round(totalPrice + tax,-1);
                totalQty += PipeQty;
                let tmpObject = {
                    date: `${receipt.date.getDate()}/${receipt.date.getMonth()+1}/${receipt.date.getFullYear()}`,
                    name: listUserIN[userIndex].username,
                    address: listUserIN[userIndex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: PipeQty,
                    productPrice: _.round(productDetail.price).currency(),
                    totalPrice: _.round(totalPrice).currency(),
                    tax: _.round(tax).currency(),
                    totalAfterTax: _.round(totalAfterTax).currency()
                };
                tableBody += Mustache.render(global.config.rowTemplate, tmpObject);


                tmpObject = {
                    name: listUserIN[userIndex].username,
                    address: listUserIN[userIndex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: PipeQty,
                    productPrice: productDetail,
                    totalPrice: totalPrice,
                    tax: tax,
                    totalAfterTax: totalAfterTax
                };
                jsonOut[receipt_number]['IN']['item'].push(tmpObject)

                totalIN += totalAfterTax;
                testTotalIN += _.round(totalAfterTax);
                productDetail.qty -= PipeQty;
                totalGasPipeQty -= PipeQty;
                UserQtyUseThisProduct--;
                totalUserIN--;
                userIndex++;
            }
        } else if (totalUserIN === 0) {
            while (productDetail.qty > 0) {
                let PipeQty = 2;
                if (PipeQty > productDetail.qty) {
                    PipeQty = productDetail.qty;
                }

                if (tmpUserList.length === 0) {
                    tmpUserList = tmpUserList.concat(processdata.userListt);
                }

                let user = _.pullAt(tmpUserList, [1])[0];

                let totalPrice = productDetail.price*PipeQty;
                let tax = productDetail.tax*PipeQty;
                let totalAfterTax = _.round(totalPrice + tax,-1);
                totalQty += PipeQty;
                let tmpObject = {
                    date: `${receipt.date.getDate()}/${receipt.date.getMonth()+1}/${receipt.date.getFullYear()}`,
                    name: listUserIN[userIndex].username,
                    address: listUserIN[userIndex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: PipeQty,
                    productPrice: _.round(productDetail.price).currency(),
                    totalPrice: _.round(totalPrice).currency(),
                    tax: _.round(tax).currency(),
                    totalAfterTax: _.round(totalAfterTax).currency()
                };
                tableBody += Mustache.render(global.config.rowTemplate, tmpObject);


                tmpObject = {
                    name: listUserIN[userIndex].username,
                    address: listUserIN[userIndex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: PipeQty,
                    productPrice: productDetail,
                    totalPrice: totalPrice,
                    tax: tax,
                    totalAfterTax: totalAfterTax
                };
                jsonOut[receipt_number]['IN']['item'].push(tmpObject)
                totalIN += totalAfterTax;
                testTotalIN += _.round(totalAfterTax);
                productDetail.qty -= PipeQty;
            } 
        } else {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: thuật toán sai. File: ${__filename}, Line: ${_}\r\n\r\n`);
        }
    });

    let userindex = 0;
    let minUserIndex = 0;
    let maxUserIndex = listUserIN.length - 1;
    let arrayGiftItems = Object.keys(GIFTitem);
    let userWithGif = [];
    arrayGiftItems.sort();
    // prepare GIFT item to write to sheet IN
    arrayGiftItems.forEach(function(productCode) {
        let productDetail = GIFTitem[productCode];
        if (productCode.match(/^KBB.*/gi) || productCode.match(/^KCS.*/gi)) {

        } else {
            minUserIndex = userindex
        }
        
        for (let i = 1; i <= productDetail.qty; i++) {
            if (typeof userWithGif[userindex] === 'undefined') {
                userWithGif[userindex] = {}
                userWithGif[userindex][productCode] = {
                    name: listUserIN[userindex].username,
                    address: listUserIN[userindex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: 1,
                    price: 0,
                    totalPrice: 0,
                    VAT: 0,
                    totalAftertax: 0,
                    note: "Hàng khuyến mãi không thu tiền"
                }
            } else if (typeof userWithGif[userindex][productCode] === 'undefined') {
                userWithGif[userindex][productCode] = {
                    name: listUserIN[userindex].username,
                    address: listUserIN[userindex].address,
                    productCode: productCode,
                    productName: productDetail.name,
                    productQty: 1,
                    price: 0,
                    totalPrice: 0,
                    VAT: 0,
                    totalAftertax: 0,
                    note: "Hàng khuyến mãi không thu tiền"
                }
            } else {
                userWithGif[userindex][productCode].productQty ++;
            }
            if (userindex < maxUserIndex) {
                userindex++;
            } else {
                userindex = minUserIndex
            }
        }
        totalQty += productDetail.qty;
        //Check empty string
        if (!productCode || !productDetail.name || !productDetail.qty) {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Dữ liệu bị rỗng ở Sheet IN\r\n\r\n`);
        }
    })
    userWithGif.forEach(function(gifts) {
        _.forOwn(gifts, function (gift) {
            tableBody += Mustache.render(global.config.rowTemplate, {
                date: `${receipt.date.getDate()}/${receipt.date.getMonth()+1}/${receipt.date.getFullYear()}`,
                name: gift.name,
                address: gift.address,
                productCode: gift.productCode,
                productName: gift.productName,
                productQty: gift.productQty,
                productPrice: '',
                totalPrice: '',
                tax: '',
                totalAfterTax: '',
                note: "Hàng khuyến mãi không thu tiền"
            });
            
            jsonOut[receipt_number]['GIFT']['item'].push(gift);
        })
    })

    let realTotalIN = (listUserKM.length > 0) ? totalIN : receipt.total;
    //write PDF page IN
    writePDF.write(
        global.config.templatePDF, 
        path.join(global.config.pdfPath, fileNameIN),
        {
            'table-data': tableBody,
            'receipt-number': receipt_number,
            'day': receipt.date.getDate(),
            'month': receipt.date.getMonth()+1,
            'year': receipt.date.getFullYear(),
            'sum-all': _.round(realTotalIN).currency(),
            'sum-qty': totalQty
        },
        "completeIN"
    );
    jsonOut[receipt_number]['IN']['total'] = realTotalIN;
    if (testTotalIN != _.round(realTotalIN)) {
        totalInDiffer = testTotalIN.differenceWith(_.round(realTotalIN));
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tổng giá tiền không giống với cộng thực tế (Kiểm tra: ${testTotalIN}, thực tế: ${_.round(realTotalIN).currency()}, sai số: ${totalInDiffer}${totalInDiffer < 10 ? ", Có thể do làm tròn" : ""})\r\n\r\n`);
    }
    // prepare item to write into sheet KM
    let tableBodyKM = "";
    let totalKM = receipt.total - _.round(realTotalIN);
    let testTotalKM = 0
    listUserKM.forEach(function (rowData, index) {
        if (rowData.productQty == 0) {
            return;
        }
        let totalPrice = rowData.price*rowData.productQty
        let tax = totalPrice/10
        let totalAfterTax = totalPrice + tax
        testTotalKM += totalAfterTax;
        tableBodyKM += Mustache.render(global.config.rowTemplate, {
            date: `${receipt.date.getDate()}/${receipt.date.getMonth()+1}/${receipt.date.getFullYear()}`,
            name: rowData.username,
            address: rowData.address,
            productCode: rowData.productCode,
            productName: rowData.productName,
            productQty: rowData.productQty,
            productPrice: _.round(rowData.price).currency(),
            totalPrice: _.round(totalPrice).currency(),
            tax: _.round(tax).currency(),
            totalAfterTax: _.round(totalAfterTax).currency()
        });
        
        jsonOut[receipt_number]['KM']['item'].push({
            name: rowData.username,
            address: rowData.address,
            productCode: rowData.productCode,
            productName: rowData.productName,
            productQty: rowData.productQty,
            price: rowData.price,
            totalPrice: totalPrice,
            VAT: tax,
            totalAftertax: totalAfterTax,
        });

        //Check empty string
        if (!rowData.username || !rowData.address || !rowData.productCode || !rowData.productName || !rowData.price || !totalPrice || !tax || !totalAfterTax) {
            fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Dữ liệu bị rỗng ở Sheet KM\r\n\r\n`);
        }
    });
    // write to KM page PDF
    if (totalKM > 0) {
        writePDF.write(
            global.config.templatePDF,
            path.join(global.config.pdfPath, fileNameKM),
            {
                'table-data': tableBodyKM,
                'receipt-number': receipt_number,
                'day': receipt.date.getDate(),
                'month': receipt.date.getMonth()+1,
                'year': receipt.date.getFullYear(),
                'sum-all': _.round(totalKM).currency(),
            },
            "completeKM"
        );
    } else {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "log.txt"), "Not have KM file: " + receipt_number + "\r\n");
        global.processLogger.emit('completeOne', "completeKM");
    }
    let extraLogStr = listUserKM.length > 0 ? " (Chưa xử lý được)" : " (Đã xử lý)";
    jsonOut[receipt_number]['KM']['total'] = totalKM;
    testTotalKM = _.round(testTotalKM);
    if (totalKM < -10) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tổng giá tiền hàng KM bị âm, giá tiền bán hàng bị sai: ${totalKM} (Âm nhiều, do thiếu sản phẩm hoặc ứng dụng lỗi) - (Đã xử lý - Cần kiểm tra lại)\r\n\r\n`);
    } else if (totalKM < 0) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tổng giá tiền hàng KM bị âm, giá tiền bán hàng bị sai: ${totalKM} (Âm ít, do làm tròn sai) - (Đã xử lý)\r\n\r\n`);
    } else if (_.round(Math.abs(testTotalKM-totalKM), 2) > 10) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tổng giá tiền KM sai nhiều: Kiểm tra: ${_.round(testTotalKM, 2)}, Thực tế: ${totalKM}, chênh lệch: ${_.round(Math.abs(testTotalKM-totalKM), 2)} - (Cần kiểm tra lại)\r\n\r\n`);
    } else if (_.round(Math.abs(testTotalKM-totalKM), 2) > 0) {
        fs.appendFileSync(path.join(__dirname, "tmp", "log", "error.txt"), `Số HĐ ${receipt_number} ngày ${formatedDate}: Tổng giá tiền KM sai (Sai ít, do làm tròn sai): Kiểm tra: ${_.round(testTotalKM, 2)}, Thực tế: ${totalKM}, chênh lệch: ${_.round(Math.abs(testTotalKM-totalKM), 2)}\r\n\r\n`);
    }
    // write to XLSX 2 sheet
    writePDF.writeXLSX(jsonOut[receipt_number]).then(function() {
       
    });
    previousReceiptIndex = receipt_number;
});