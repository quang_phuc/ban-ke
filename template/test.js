const fs = require("fs");
const path = require("path");
const _ = require("lodash")
const XLSX = require("xlsx-style");
var wkhtmltopdf = require('wkhtmltopdf');

outputFileName = 'test.pdf';
wkhtmltopdf("file:///D:/Node/ban-ke/template/template.html", {
    output: outputFileName,
    printMediaType: true,
    marginLeft: '15mm',
    marginRight : '15mm',
    marginTop : '15mm',
    marginBottom : '15mm',
    pageOffset: 0,
    footerCenter: 'page [page]/[toPage]',
    orientation: "Landscape",

}, function() {
    console.log(path.basename(outputFileName, '.pdf'));
});