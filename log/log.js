const fs = require("fs");
const path = require("path");
const logpath = __dirname;
function writeLog(filename, message) {
    fs.appendFile(path.join(logpath, filename), message + "\n", function(err) {
        if (err) throw err;
        console.log('Loged error');
    });
}
module.exports = {
    writeLog
}