module.exports = require('yargs')
.option('f', {
    alias: 'file',
    demandOption: true,
    describe: 'File xlsx to work',
    type: 'string'
})
.help()
.argv